// --------------------------------------------------------------------------------------------------------------------

// core
const path = require('path')

// --------------------------------------------------------------------------------------------------------------------
// setup

const account = {}

function nt(callback, err, data) {
  process.nextTick(() => {
    callback(err, data)
  })
}

// --------------------------------------------------------------------------------------------------------------------

function createAccount(email, callback) {
  if ( !account[email] ) {
    const now = Date.now()
    account[email] = {
      email,
      logins   : 0,
      inserted : now,
      updated  : now,
    }
  }
  nt(callback, null, account[email])
}

function incrementLogins(email, callback) {
  if ( !account[email] ) {
    return nt(callback, new Error(`No account found for email ${email}`))
  }
  account[email].logins++
  nt(callback, null, account[email])
}

function login(email, callback) {
  nt(callback, null, account[email])
}

// --------------------------------------------------------------------------------------------------------------------

module.exports = {
  createAccount,
  incrementLogins,
  login,
}

// --------------------------------------------------------------------------------------------------------------------
